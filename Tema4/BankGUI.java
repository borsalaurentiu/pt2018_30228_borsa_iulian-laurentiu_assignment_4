package Tema4;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class BankGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JButton clientiButton = new JButton("Clienti");
	private JButton conturiButton = new JButton("Conturi");
	private JButton retrageButton = new JButton("Retrage numerar");
	private JButton depuneButton = new JButton("Depune numerar");

	private JLabel titularLabel = new JLabel("ID titular: ");
	private JLabel contLabel = new JLabel("ID cont: ");
	private JLabel sumaLabel = new JLabel("Suma: ");

	private JTextField titularText = new JTextField();
	private JTextField contText = new JTextField();
	private JTextField sumaText = new JTextField();



	public BankGUI() {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		adaugareAscultatori();
	}

	public void initializareEcran() {
		setTitle("Tema 4 - Banca");
		setSize(new Dimension(750, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
	}

	public void initializareComponente() {
		clientiButton.setBounds(100, 50, 150, 50);
		conturiButton.setBounds(500, 50, 150, 50);
		depuneButton.setBounds(300, 50, 150, 50);
		retrageButton.setBounds(300, 125, 150, 50);
		titularLabel.setBounds(300, 200, 75, 30);
		titularText.setBounds(375, 200, 75, 30);
		contLabel.setBounds(300, 250, 75, 30);
		contText.setBounds(375, 250, 75, 30);
		sumaLabel.setBounds(300, 300, 75, 30);
		sumaText.setBounds(375, 300, 75, 30);
		titularLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		contLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		sumaLabel.setHorizontalAlignment(SwingConstants.RIGHT);
	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(clientiButton);
		panel.add(conturiButton);
		panel.add(retrageButton);
		panel.add(depuneButton);
		panel.add(sumaLabel);
		panel.add(titularLabel);
		panel.add(contLabel);
		panel.add(sumaText);
		panel.add(titularText);
		panel.add(contText);
	}

	public void adaugareAscultatori() {

		clientiButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PersonGUI personGUI;
				personGUI = new PersonGUI();
				personGUI.setVisible(true);
			}
		});

		conturiButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AccountGUI conturiGUI;
				conturiGUI = new AccountGUI();
				conturiGUI.setVisible(true);

			}
		});

		depuneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int idT = Integer.parseInt(titularText.getText());
					int idC = Integer.parseInt(contText.getText());
					int sold = Integer.parseInt(sumaText.getText());
					Bank banca = new Bank();
					Person titular = new Person(idT, null, null, null);
					Account cont = new Account(idC, 0, 0, null);
					banca.addMoney(titular, cont, sold);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		retrageButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int idT = Integer.parseInt(titularText.getText());
					int idC = Integer.parseInt(contText.getText());
					int sold = Integer.parseInt(sumaText.getText());
					Bank banca = new Bank();
					Person titular = new Person(idT, null, null, null);
					Account cont = new Account(idC, 0, 0, null);
					banca.withdrawMoney(titular, cont, sold);
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args) {
		new BankGUI();
	}
}
