package Tema4;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;


public class PersonGUI extends JFrame{
	private static final long serialVersionUID = 1L;
	private static int row;
	private static int column;
	private static int id;
	private static String nume = new String();
	private static String adresa = new String();
	private static String CNP = new String();

	private JPanel panel = new JPanel();
	private JTable tabel = new JTable();

	private JTextField stergeIdText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField numeText = new JTextField();
	private JTextField CNPText = new JTextField();
	private JTextField adresaText = new JTextField();
	private JTextField reportText = new JTextField();

	private JLabel idLabel = new JLabel("ID: ");
	private JLabel numeLabel = new JLabel("Nume: ");
	private JLabel CNPLabel = new JLabel("CNP: ");
	private JLabel adresaLabel = new JLabel("Adresa: ");
	private JLabel stergeLabel = new JLabel("Se va sterge titularul dupa ID.");

	private JButton adaugaButton = new JButton("Adauga");
	private JButton stergeButton = new JButton("Sterge");
	private JButton editeazaButton = new JButton("Editeaza");

	public PersonGUI() {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		try {
			Bank banca = new Bank();
			adaugareTabel(banca.getPersons());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		adaugareAscultatori();
	}

	public void initializareEcran() {
		setTitle("Tema 4 - Titulari");
		setSize(new Dimension(650, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		add(panel);
	}

	public void initializareComponente() {

		idText.setBounds(125, 25, 90, 30);
		numeText.setBounds(125, 75, 90, 30);
		CNPText.setBounds(125, 125, 90, 30);
		adresaText.setBounds(125, 175, 90, 30);
		stergeIdText.setBounds(125, 360, 90, 30);
		reportText.setBounds(240, 360, 375, 30);		
		reportText.setEditable(false);

		idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		CNPLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		adresaLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		stergeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stergeIdText.setHorizontalAlignment(SwingConstants.CENTER);
		reportText.setHorizontalAlignment(SwingConstants.CENTER);

		idLabel.setBounds(25, 25, 90, 30);
		numeLabel.setBounds(25, 75, 90, 30);
		CNPLabel.setBounds(25, 125, 90, 30);
		adresaLabel.setBounds(25, 175, 90, 30);
		stergeLabel.setBounds(25, 315, 190, 30);

		adaugaButton.setBounds(25, 270, 90, 30);
		editeazaButton.setBounds(125, 270, 90, 30);
		stergeButton.setBounds(25, 360, 90, 30);

		tabel.setBounds(240, 25, 375, 310);


	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(adaugaButton);
		panel.add(stergeButton);
		panel.add(editeazaButton);

		panel.add(stergeIdText);
		panel.add(idText);
		panel.add(numeText);
		panel.add(CNPText);
		panel.add(adresaText);
		panel.add(idLabel);
		panel.add(numeLabel);
		panel.add(CNPLabel);
		panel.add(adresaLabel);
		panel.add(stergeLabel);
		panel.add(reportText);
		panel.add(tabel);
	}



	public void adaugareTabel(Set<Person> titulari)  {
		String[] coloane = {"ID","Titular","CNP","Adresa"};		
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("Id");
		model.addColumn("Titular");
		model.addColumn("CNP");
		model.addColumn("Adresa");
		model.addRow(coloane);	

		Object[] row = new Object[4];
		for(Person titular: titulari) {
			row[0] = titular.getId();
			row[1] = titular.getNume();
			row[2] = titular.getCNP(); 
			row[3] = titular.getAdresa(); 
			model.addRow(row);
		}
		tabel.setModel(model);
	}




	public void adaugareAscultatori() {
		adaugaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Person titular = new Person();
				titular.setId(Integer.parseInt(idText.getText()));
				titular.setNume(numeText.getText());
				titular.setCNP(CNPText.getText());
				titular.setAdresa(adresaText.getText());

				try {
					Bank banca = new Bank();
					banca.addPerson(titular);					
					adaugareTabel(banca.getPersons());
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				String report = "Titularul a fost adaugat cu succes!";
				reportText.setText(report);
			}
		});

		editeazaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					nume = (String) tabel.getValueAt(row, 1);
					CNP = (String) tabel.getValueAt(row, 2);
					adresa = (String) tabel.getValueAt(row, 3);
					Bank banca = new Bank();
					banca.updatePerson(new Person(id, nume, CNP, adresa));
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		stergeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {


				Person titular = new Person(Integer.parseInt(stergeIdText.getText()), null, null, null);

				try {
					Bank banca = new Bank();
					banca.removePerson(titular);				

					adaugareTabel(banca.getPersons());
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				String report = "Titularul a fost sters cu succes!";
				reportText.setText(report);												
			}
		});

		tabel.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				row = tabel.rowAtPoint(evt.getPoint());
				column = tabel.columnAtPoint(evt.getPoint());
				if(row >= 0 && column >= 0) {
					id = row;
				}
			}
		});

	}

	public static void main(String[] args){
		new PersonGUI();
	}

}

