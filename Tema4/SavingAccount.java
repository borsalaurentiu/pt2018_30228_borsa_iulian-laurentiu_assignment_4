package Tema4;

public class SavingAccount  extends Account {

	private static final long serialVersionUID = 1L;
	private double dobanda = 0.5;
	private static String tip = new String("Saving Account");
	
	public SavingAccount(int id, int titular, double sold) {
		super(id, titular, sold, tip);
	}

	public void depozitare(int suma){
		sold = sold + suma + suma*dobanda;
	}
	
}
