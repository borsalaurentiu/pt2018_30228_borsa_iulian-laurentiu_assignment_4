package Tema4;

import java.io.Serializable;
import java.util.Observable;

public class Account  extends Observable implements Serializable{

	private static final long serialVersionUID = 1L;
	
	protected int id;
	protected int titular;
	protected double sold = 0;
	protected String tip;
	public Account() {
		
	}
	public Account(int id, int titular, double sold, String tip){
		this.id = id;
		this.sold = sold;
		this.titular = titular;
		this.tip = tip;
	}
	
	public void retragere(int suma){ 
		sold = sold - suma;
		setChanged();
		notifyObservers();
	}
	
	public void depozitare(int suma){
		sold = sold + suma;
		setChanged();
		notifyObservers();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getTitular() {
		return titular;
	}

	public void setTitular(int titular) {
		this.titular = titular;
	}

	public double getSold() {
		return sold;
	}
	public void setSold(double sold) {
		this.sold = sold;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	
	@Override
	public String toString() {
		return "ID: " + id + ", ID Titular " + titular + ", Sold " + sold + ", Tip " + tip + "\n";
	}

	
}
