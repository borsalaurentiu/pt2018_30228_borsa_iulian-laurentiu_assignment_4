package Tema4;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;


public class AccountGUI extends JFrame{
	private static final long serialVersionUID = 1L;

	private JPanel panel = new JPanel();
	private JTable tabel = new JTable();

	private JTextField stergeIdText = new JTextField();
	private JTextField idText = new JTextField();
	private JTextField numeText = new JTextField();
	private JTextField soldText = new JTextField();
	private JTextField reportText = new JTextField();

	private JLabel idLabel = new JLabel("ID: ");
	private JLabel numeLabel = new JLabel("ID titular: ");
	private JLabel soldLabel = new JLabel("Sold: ");
	private JLabel stergeLabel = new JLabel("Se va sterge contul dupa ID.");

	private JButton adaugaButton = new JButton("Adauga");
	private JButton stergeButton = new JButton("Sterge");
	private JButton editeazaButton = new JButton("Editeaza");

	private JRadioButton savingRadioButton = new JRadioButton("Saving account");
	private JRadioButton spendingRadioButton = new JRadioButton("Spending account");
	private ButtonGroup group = new ButtonGroup();

	public static Person c = new Person();
	public AccountGUI() {
		initializareEcran();
		adaugareComponente();
		initializareComponente();
		Bank banca;
		try {
			banca = new Bank();
			adaugareTabel(banca.getAccounts());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		adaugareAscultatori();
	}

	public void initializareEcran() {
		setTitle("Tema 4 - Conturi");
		setSize(new Dimension(650, 450));
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		add(panel);
	}

	public void initializareComponente() {

		stergeIdText.setBounds(125, 360, 90, 30);
		idText.setBounds(125, 25, 90, 30);
		numeText.setBounds(125, 75, 90, 30);
		soldText.setBounds(125, 125, 90, 30);
		reportText.setBounds(240, 360, 375, 30);		
		reportText.setEditable(false);

		idLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		numeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		soldLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		stergeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		stergeIdText.setHorizontalAlignment(SwingConstants.CENTER);
		reportText.setHorizontalAlignment(SwingConstants.CENTER);

		idLabel.setBounds(25, 25, 90, 30);
		numeLabel.setBounds(25, 75, 90, 30);
		soldLabel.setBounds(25, 125, 90, 30);
		stergeLabel.setBounds(25, 315, 190, 30);

		adaugaButton.setBounds(25, 270, 90, 30);
		editeazaButton.setBounds(125, 270, 90, 30);
		stergeButton.setBounds(25, 360, 90, 30);
		tabel.setBounds(240, 25, 375, 310);

		savingRadioButton.setBounds(25, 175, 150, 30);
		spendingRadioButton.setBounds(25, 225, 150, 30);
		savingRadioButton.setActionCommand("saving");
		spendingRadioButton.setActionCommand("spending");
		group.add(savingRadioButton);
		group.add(spendingRadioButton);

	}

	public void adaugareComponente() {
		panel.setLayout(null);
		panel.add(adaugaButton);
		panel.add(stergeButton);
		panel.add(editeazaButton);

		panel.add(stergeIdText);
		panel.add(idText);
		panel.add(numeText);
		panel.add(soldText);
		panel.add(idLabel);
		panel.add(numeLabel);

		panel.add(soldLabel);
		panel.add(stergeLabel);
		panel.add(reportText);
		panel.add(tabel);
		panel.add(savingRadioButton);
		panel.add(spendingRadioButton);
	}



	public void adaugareTabel(ArrayList<Account> conturi)  {
		String[] coloane = {"ID","ID Titular","Sold","Tip"};		
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("ID");
		model.addColumn("ID Titular");
		model.addColumn("Sold");
		model.addColumn("Tip");
		model.addRow(coloane);	

		Object[] row = new Object[4];
		for(Account cont: conturi) {
			row[0] = cont.getId();
			row[1] = cont.getTitular();
			row[2] = cont.getSold(); 
			row[3] = cont.getTip(); 
			model.addRow(row);
		}

		tabel.setModel(model);
	}


	public void adaugareAscultatori() {
		adaugaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String report = "Contul a fost adaugat cu succes!";
				if(savingRadioButton.isSelected())
				{
					int id = Integer.parseInt(idText.getText());
					int idTitular = Integer.parseInt(numeText.getText());
					double sold = Double.parseDouble(soldText.getText());

					Account savingAccount = new SavingAccount(id, idTitular, sold);

					try {
						Bank banca = new Bank();
						banca.addAssociatedAccount(new Person(idTitular, null, null, null), savingAccount);
						adaugareTabel(banca.getAccounts());
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
				}
				else
				{
					int id = Integer.parseInt(idText.getText());
					int idTitular = Integer.parseInt(numeText.getText());
					double sold = Double.parseDouble(soldText.getText());

					Account spendingAccount = new SpendingAccount(id, idTitular, sold);
					try {
						Bank banca = new Bank();
						banca.addAssociatedAccount(new Person(idTitular, null, null, null), spendingAccount);
						adaugareTabel(banca.getAccounts());
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}


				}
				reportText.setText(report);
			}
		});

		editeazaButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});

		stergeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Account cont = new Account(Integer.parseInt(stergeIdText.getText()), 0, 0, null);
				try {
					Bank banca = new Bank();
					banca.removeAssociatedAccount(new Person(Integer.parseInt(numeText.getText()), null, null, null), cont);
					adaugareTabel(banca.getAccounts());
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args){
		new AccountGUI();
	}

}

