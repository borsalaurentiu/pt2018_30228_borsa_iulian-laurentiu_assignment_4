package Tema4;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable,Observer{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nume;
	private String CNP;
	private String adresa;
	
	public Person() {
		
	}
	public Person(int id, String nume, String CNP, String adresa){
		this.id = id;
		this.nume = nume;
		this.CNP = CNP;
		this.adresa = adresa;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getCNP() {
		return CNP;
	}

	public void setCNP(String CNP) {
		this.CNP = CNP;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public void update(Observable o, Object arg) {
		
	}

	@Override
	public String toString() {
		return "\nID: " + id + ", nume: " + nume + ", CNP: " + CNP + ", adresa: " + adresa;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(!(obj instanceof Person)) {
			return false;
		}
		Person person = (Person) obj;
		
		return Integer.compare(id, person.id) == 0;
	}

	
}
