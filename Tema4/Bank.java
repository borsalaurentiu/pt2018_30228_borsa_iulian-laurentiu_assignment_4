package Tema4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Bank implements BankProc,Serializable {

	private static final long serialVersionUID = 1L;
	public static String fisier = new String("banca.ser");
	Map <Person, ArrayList<Account>> banca = new HashMap<Person, ArrayList<Account>>();

	public Bank() throws IOException, ClassNotFoundException {
		FileInputStream file = new FileInputStream(fisier);
		ObjectInputStream in = new ObjectInputStream(file);
		banca = (Map<Person, ArrayList<Account>>) in.readObject();
		in.close();
		file.close();
	}

	public Map<Person, ArrayList<Account>> getHashMap() {
		return banca;
	}

	public void addPerson(Person person) {
		assert person!=null;
		int size = banca.size();
		if(banca.containsKey(person))
			System.out.println("Cheia este folosita deja!");
		banca.put(person, new ArrayList<Account>());

		try {
			writeData();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assert size + 1 == banca.size();
	}

	public void updatePerson(Person person) {
		assert !banca.isEmpty();
		int size = banca.size();

		Set<Person> titulari = banca.keySet();
		for(Person titular: titulari)
			if(titular.getId() == person.getId())
			{
				titular.setAdresa(person.getAdresa());
				titular.setCNP(person.getCNP());
				titular.setNume(person.getNume());
			}

		try {
			writeData();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assert size == banca.size();		
	}
	
	public void removePerson(Person person) {
		assert !banca.isEmpty();
		int size = banca.size();

		banca.remove(person);

		try {
			writeData();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assert size - 1 == banca.size();
	}

	public void addAssociatedAccount(Person person, Account account) {
		assert account!=null;
		int size = banca.size();
		ArrayList<Account> conturi = banca.get(person);
		Person persoana = new Person();
		Set<Person> titulari = banca.keySet();
		for(Person titular: titulari)
			if(titular.equals(person)) {
				persoana.setId(titular.getId());
				persoana.setNume(titular.getNume());
				persoana.setCNP(titular.getCNP());
				persoana.setAdresa(titular.getAdresa());
			}
		conturi.add(account);

		banca.remove(person);
		banca.put(persoana, conturi);

		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assert size+1 == banca.size();
	}

	public void removeAssociatedAccount(Person person, Account account) {
		assert account!=null;
		int size = banca.size();
		ArrayList<Account> conturi = banca.get(person);
		ArrayList<Account> conturiAux = new ArrayList<Account>();
		Person persoana = new Person();
		Set<Person> titulari = banca.keySet();
		for(Person titular: titulari)
			if(titular.equals(person)) {
				persoana.setId(titular.getId());
				persoana.setNume(titular.getNume());
				persoana.setCNP(titular.getCNP());
				persoana.setAdresa(titular.getAdresa());
			}
		for(Account cont: conturi)
			if(cont.getId() != account.getId())
				conturiAux.add(cont);

		banca.remove(person);
		banca.put(persoana, conturiAux);

		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assert size+1 == banca.size();

	}

	public Set<Person> getPersons(){
		Set<Person> titulari = banca.keySet();
		return titulari;	
	}

	public ArrayList<Account> getAccounts(){
		ArrayList<Account> conturi = new ArrayList<Account>(); 
		Set<Person> titulari = banca.keySet();
		for(Person titular: titulari) {
			ArrayList<Account> contAux = banca.get(titular);
			for(Account cont: contAux) {
				conturi.add(cont);
			}
		}
		return conturi;	
	}

	public void writeData() throws IOException, ClassNotFoundException{
		FileOutputStream file = new FileOutputStream(fisier);
		ObjectOutputStream out = new ObjectOutputStream(file);
		out.writeObject(banca);
		out.close();
		file.close();
	}

	public void addMoney(Person person, Account account, int sold) {
		assert account!=null;
		int size = banca.size();
		ArrayList<Account> conturi = banca.get(person);

		for(Account cont: conturi)
			if(cont.getId() == account.getId())
				cont.depozitare(sold);
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assert size+1 == banca.size();
	}

	public void withdrawMoney(Person person, Account account, int sold) {
		assert account!=null;
		int size = banca.size();
		ArrayList<Account> conturi = banca.get(person);
		
		for(Account cont: conturi)
			if(cont.getId() == account.getId())
			cont.retragere(sold);
		try {
			writeData();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assert size+1 == banca.size();
	}

}