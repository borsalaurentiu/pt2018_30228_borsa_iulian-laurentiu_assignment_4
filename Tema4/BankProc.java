package Tema4;

import java.util.ArrayList;
import java.util.Set;

public interface BankProc {
	
	/**
	 * @pre person !=null
	 * @post getSize()=getSize()@pre+1
	 */
	public void addPerson(Person person);
	
	/**
	 * @pre person !=null
	 * @post getSize()=getSize()@pre
	 */
	public void updatePerson(Person person);
	
	/**
	 * @pre !isEmpyy()
	 * @post getSize()=getSize()@pre-1
	 */
	public void removePerson(Person person);
	
	
	
	public void addAssociatedAccount(Person person, Account account);
	public void removeAssociatedAccount(Person person, Account account);
	public Set<Person> getPersons();
	public ArrayList<Account> getAccounts();
	public void addMoney(Person person, Account account, int sold);
	public void withdrawMoney(Person person, Account account, int sold);
	
}
